
// KodirovanieDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "Kodirovanie.h"
#include "KodirovanieDlg.h"
#include "afxdialogex.h"
#include <iostream>
#include <string>
#include <fstream>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;
// ���������� ���� CKodirovanieDlg



CKodirovanieDlg::CKodirovanieDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CKodirovanieDlg::IDD, pParent)
	, InitalText(_T(""))
	, ConvertedText(_T(""))
	, Key(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CKodirovanieDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_Inital_text, InitalText);
	DDX_Text(pDX, IDC_Converted_text, ConvertedText);
	DDX_Text(pDX, IDC_Key, Key);
	DDX_Control(pDX, IDC_RADIO2, zahifr);
	DDX_Control(pDX, IDC_RADIO3, rashifr);
}

BEGIN_MESSAGE_MAP(CKodirovanieDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Generate_key, &CKodirovanieDlg::OnBnClickedGeneratekey)
	ON_BN_CLICKED(IDC_Download, &CKodirovanieDlg::OnBnClickedDownload)
	ON_BN_CLICKED(IDC_Do, &CKodirovanieDlg::OnBnClickedDo)
	ON_BN_CLICKED(IDC_Unload, &CKodirovanieDlg::OnBnClickedUnload)
END_MESSAGE_MAP()


// ����������� ��������� CKodirovanieDlg

BOOL CKodirovanieDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CKodirovanieDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CKodirovanieDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

int randomRange(int low, int high)
{
	return rand() % (high - low + 1) + low;
}

void CKodirovanieDlg::OnBnClickedGeneratekey()
{
	char* newKey=new char[8];
	memset(newKey, 0, 8*sizeof(char));
	Key = "";
	srand(time(NULL));
	for (int n = 0; n < 7;n++)
	{
		newKey[n] = randomRange('a', 'z');
		Key += newKey[n];
	}
	//string str(newKey);
	UpdateData(false);
}


void CKodirovanieDlg::OnBnClickedDownload()
{
	CFileDialog fileDialog(TRUE, NULL, L"*.txt");
	int res = fileDialog.DoModal();
	if (res != IDOK)
		return;
	CFile file;
	file.Open(fileDialog.GetPathName(), CFile::modeRead);
	CStringA str;
	LPSTR pBuf = str.GetBuffer(file.GetLength() + 1);
	file.Read(pBuf, file.GetLength() + 1);
	pBuf[file.GetLength()] = NULL;
	//CStringA decodedText = str;
	InitalText = str;
	file.Close();
	str.ReleaseBuffer();
	UpdateData(FALSE);
}


void CKodirovanieDlg::OnBnClickedDo()
{
	UpdateData(true);
	if (zahifr.GetCheck())
	{
		ConvertedText = " ";
		string res;

		string data;
		data.resize(InitalText.GetLength());
		WideCharToMultiByte(CP_ACP, 0, InitalText, -1, &data[0], data.size(), NULL, NULL);

		string key;
		key.resize(Key.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Key, -1, &key[0], key.size(), NULL, NULL);

		res.resize(data.length());
		int additional = 0;
		int dif = data.length() % key.length();
		if (dif != 0)
		{
			additional = key.length() - dif;
			additional %= key.length();
			data.resize(InitalText.GetLength() + additional);
			res.resize(data.length() + additional);
			for (int i = 0; i < data.length(); i++)
			{
				res[i] = data[i] ^ key[i%key.length()];
				//ConvertedText += res[i];
			}
		}
		else
		{
			for (int i = 0; i < data.length(); i++)
		{
			res[i] = data[i] ^ key[i%key.length()];
			//ConvertedText += res[i];
		} 
			
		}
		ConvertedText = res.c_str();

	}
	if (rashifr.GetCheck())
	{
		InitalText = ConvertedText;

		string resOut;

		string data;
		data.resize(InitalText.GetLength());
		WideCharToMultiByte(CP_ACP, 0, InitalText, -1, &data[0], data.size(), NULL, NULL);

		string key;
		key.resize(Key.GetLength());
		WideCharToMultiByte(CP_ACP, 0, Key, -1, &key[0], key.size(), NULL, NULL);

		resOut.resize(data.length());
			for (int i = 0; i < data.length(); i++)
			{
				resOut[i] = data[i] ^ key[i%key.length()];
				//ConvertedText += res[i];
			}
			ConvertedText = resOut.c_str();



	}
	UpdateData(false);
}


void CKodirovanieDlg::OnBnClickedUnload()
{
	std::ofstream out;
	out.open("������������� �����.txt");
	if (out.is_open())
	{
		out << ConvertedText << std::endl;
	}
}
