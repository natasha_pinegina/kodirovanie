
// KodirovanieDlg.h : ���� ���������
//

#pragma once
#include "afxwin.h"


// ���������� ���� CKodirovanieDlg
class CKodirovanieDlg : public CDialogEx
{
// ��������
public:
	CKodirovanieDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_KODIROVANIE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString InitalText;
	CString ConvertedText;
	CString Key;
	afx_msg void OnBnClickedGeneratekey();
	afx_msg void OnBnClickedDownload();
	afx_msg void OnBnClickedDo();
	afx_msg void OnBnClickedUnload();
	CButton zahifr;
	CButton rashifr;
	CString kyenew;
};
