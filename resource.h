//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Kodirovanie.rc
//
#define IDD_KODIROVANIE_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_Inital_text                 1000
#define IDC_Converted_text              1001
#define IDC_Download                    1002
#define IDC_Unload                      1003
#define IDC_Do                          1004
#define IDC_Decrypt                     1006
#define IDC_Key                         1007
#define IDC_Generate_key                1008
#define IDC_RADIO2                      1009
#define IDC_RADIO3                      1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
